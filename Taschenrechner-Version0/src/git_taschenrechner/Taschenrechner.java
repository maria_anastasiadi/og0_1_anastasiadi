package git_taschenrechner;

public class Taschenrechner {

	// TODO Auto-generated method stubpackage git_taschenrechner;

	public static double add(double zahl1, double zahl2) {
		return zahl1 + zahl2;
	}

	public static double sub(double zahl1, double zahl2) {
		return zahl1 - zahl2;
	}

	public static double mul(double zahl1, double zahl2) {
		return zahl1 * zahl2;
	}

	public static double div(double zahl1, double zahl2) {
		return zahl1 / zahl2;
	}

}
