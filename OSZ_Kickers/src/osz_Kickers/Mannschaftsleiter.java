package osz_Kickers;

public class Mannschaftsleiter extends Spieler {

	private String nameMannschaft;
	private double rabatt;
	
	public Mannschaftsleiter() {
		super();
		
	}
	
	public Mannschaftsleiter(String nameMannschaft, double rabatt, int trikotnummer, String spielposition, String name, int telefonnummer, boolean jahresbeitragBezahlt) {
		super(trikotnummer, spielposition, name, telefonnummer, jahresbeitragBezahlt);
		this.nameMannschaft = nameMannschaft;
		this.rabatt = rabatt;
	}

	public String getNameMannschaft() {
		return nameMannschaft;
	}

	public void setNameMannschaft(String nameMannschaft) {
		this.nameMannschaft = nameMannschaft;
	}

	public double getRabatt() {
		return rabatt;
	}

	public void setRabatt(double rabatt) {
		this.rabatt = rabatt;
	}
	
}
