package osz_Kickers;

public abstract class Person {

		private String name ;
		private int telefonnummer;
		private boolean jahresbeitragBezahlt;
		
		public Person () {
			
		}
		
		public Person(String name, int telefonnummer, boolean jahresbeitragBezahlt) {
			this.name = name;
			this.telefonnummer = telefonnummer;
			this.jahresbeitragBezahlt = jahresbeitragBezahlt;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getTelefonnummer() {
			return telefonnummer;
		}

		public void setTelefonnummer(int telefonnummer) {
			this.telefonnummer = telefonnummer;
		}

		public boolean isJahresbeitragBezahlt() {
			return jahresbeitragBezahlt;
		}

		public void setJahresbeitragBezahlt(boolean jahresbeitragBezahlt) {
			this.jahresbeitragBezahlt = jahresbeitragBezahlt;
		}
		
}
