package osz_Kickers;

public class Schiedsrichter extends Person {

private int gepfiffenenSpiele;
	
	public Schiedsrichter () {
		super();
		
	}
	
	public Schiedsrichter(int gepfiffenenSpiele, String name, int telefonnummer, boolean jahresbeitragBezahlt) {
		super(name, telefonnummer, jahresbeitragBezahlt);
		this.gepfiffenenSpiele = gepfiffenenSpiele;
	}

	public int getGepfiffenenSpiele() {
		return gepfiffenenSpiele;
	}

	public void setGepfiffenenSpiele(int gepfiffenenSpiele) {
		this.gepfiffenenSpiele = gepfiffenenSpiele;
	}
	
	
}
