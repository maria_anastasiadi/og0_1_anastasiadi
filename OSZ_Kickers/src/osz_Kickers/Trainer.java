package osz_Kickers;

public class Trainer extends Person {
	private char lizenzklasse;
	private double aufwantsentschaedigung;
	
	public Trainer() {
		super();
		
	}
	
	public Trainer(char lizenzklasse, double aufwantsentschaedigung, String name, int telefonnummer, boolean jahresbeitragBezahlt) {
		super(name, telefonnummer, jahresbeitragBezahlt);
		this.lizenzklasse = lizenzklasse;
		this.aufwantsentschaedigung = aufwantsentschaedigung;
		
	}

	public char getLizenzklasse() {
		return lizenzklasse;
	}

	public void setLizenzklasse(char lizenzklasse) {
		this.lizenzklasse = lizenzklasse;
	}

	public double getAufwantsentschaedigung() {
		return aufwantsentschaedigung;
	}

	public void setAufwantsentschaedigung(double aufwantsentschaedigung) {
		this.aufwantsentschaedigung = aufwantsentschaedigung;
	}

}
