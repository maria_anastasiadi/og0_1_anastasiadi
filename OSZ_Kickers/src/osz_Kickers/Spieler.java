package osz_Kickers;

public class Spieler extends Person {

		private int trikotnummer;
		private String spielposition;
		
		public Spieler () {
			super();
			
		}
		
		public Spieler(int trikotnummer, String spielposition, String name, int telefonnummer, boolean jahresbeitragBezahlt) {
			super(name, telefonnummer, jahresbeitragBezahlt);
			this.trikotnummer = trikotnummer;
			this.spielposition = spielposition;
			
		}

		public int getTrikotnummer() {
			return trikotnummer;
		}

		public void setTrikotnummer(int trikotnummer) {
			this.trikotnummer = trikotnummer;
		}

		public String getSpielposition() {
			return spielposition;
		}

		public void setSpielposition(String spielposition) {
			this.spielposition = spielposition;
		}
}
