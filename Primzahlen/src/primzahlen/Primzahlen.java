package primzahlen;
import java.util.Scanner;
import java.time.Duration; 
import java.time.Instant;

public class Primzahlen {
	public static void main (String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		boolean ergebnis = false;;
		long vorher;
		long nachher;
		int i = 0;
		int d = 0;
		
		System.out.println("Gib eine Zahl ein: ");
		long eingabe = scanner.nextLong();
		while(i<5) {
			vorher = System.currentTimeMillis();
			ergebnis = isPrimzahl(eingabe);
			nachher = System.currentTimeMillis();
			System.out.println(nachher - vorher + " Millisekunden");	
			i++;
			d += nachher - vorher;
		}
		d = d / 5;

		System.out.println(eingabe + " ist " + (!ergebnis?"k":"")+ "eine Primzahl");
		System.out.println("Durchschnitt: " + d);
		
	
	}
	
	public static boolean isPrimzahl(long zahl) {
		long c = (zahl/2);
		long wert;
		while (c != 1) {
		wert = zahl%c;
		if (wert == 0) {
			return false;
		}
			c--;
		}
		return true;
	}
}
