package tauglichkeit;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Dimension;
import javax.swing.JTextArea;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JRadioButton;
import java.awt.Insets;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.SwingConstants;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class GUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		panel.setPreferredSize(new Dimension(10, 120));
		contentPane.add(panel, BorderLayout.SOUTH);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblNewLabel = new JLabel("Test");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.anchor = GridBagConstraints.NORTH;
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 0;
		panel.add(lblNewLabel, gbc_lblNewLabel);
		
		JRadioButton rdbtnReaktionButton = new JRadioButton("Reaktion");
		rdbtnReaktionButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_rdbtnReaktionButton = new GridBagConstraints();
		gbc_rdbtnReaktionButton.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnReaktionButton.gridx = 1;
		gbc_rdbtnReaktionButton.gridy = 1;
		panel.add(rdbtnReaktionButton, gbc_rdbtnReaktionButton);
		
		JButton btnStart = new JButton("Start");
		btnStart.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
		GridBagConstraints gbc_btnStart = new GridBagConstraints();
		gbc_btnStart.gridheight = 2;
		gbc_btnStart.insets = new Insets(0, 0, 5, 5);
		gbc_btnStart.gridx = 8;
		gbc_btnStart.gridy = 1;
		panel.add(btnStart, gbc_btnStart);
		
		JButton btnStopp = new JButton("Stopp");
		btnStopp.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
		GridBagConstraints gbc_btnStopp = new GridBagConstraints();
		gbc_btnStopp.gridheight = 2;
		gbc_btnStopp.insets = new Insets(0, 0, 5, 0);
		gbc_btnStopp.gridx = 11;
		gbc_btnStopp.gridy = 1;
		panel.add(btnStopp, gbc_btnStopp);
		
		JRadioButton rdbtnEinschaetzungButton = new JRadioButton("Einsch\u00E4tzung");
		rdbtnEinschaetzungButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_rdbtnEinschaetzungButton = new GridBagConstraints();
		gbc_rdbtnEinschaetzungButton.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnEinschaetzungButton.gridx = 1;
		gbc_rdbtnEinschaetzungButton.gridy = 2;
		panel.add(rdbtnEinschaetzungButton, gbc_rdbtnEinschaetzungButton);
		
		JRadioButton rdbtnKonzentrationButton = new JRadioButton("Konzentration");
		rdbtnKonzentrationButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_rdbtnKonzentrationButton = new GridBagConstraints();
		gbc_rdbtnKonzentrationButton.insets = new Insets(0, 0, 0, 5);
		gbc_rdbtnKonzentrationButton.gridx = 1;
		gbc_rdbtnKonzentrationButton.gridy = 3;
		panel.add(rdbtnKonzentrationButton, gbc_rdbtnKonzentrationButton);
		
		JTextArea textArea = new JTextArea();
		textArea.setPreferredSize(new Dimension(280, 22));
		contentPane.add(textArea, BorderLayout.EAST);
		
		JEditorPane editorPane = new JEditorPane();
		editorPane.setPreferredSize(new Dimension(280, 20));
		contentPane.add(editorPane, BorderLayout.WEST);
	}

}
