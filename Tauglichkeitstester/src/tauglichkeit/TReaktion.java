package tauglichkeit;

public class TReaktion extends TTest {

	// Attribute
	protected TStoppuhr uhr;
	protected TAmpel ampel;

	// Methoden
	public TReaktion(TCanvas canvas, TRect rechteck) {
		super(canvas, rechteck);
		// TODO Auto-generated constructor stub
	}
	
	/*
	protected void warten() {
		
	}
	*/

	@Override
	public String zeigeHilfe() {
		return "Nach dem Betätigen der Start-Schaltfläche erscheint eine Ampel, die rot zeigt. Beim Umspringen auf grün muss die Stopp-Schaltfläche gedrückt werden. Es wird anschließend die Reaktionszeit in ms angezeigt.";
	}

	@Override
	public void starten() {
		// TODO Auto-generated method stub

	}

	@Override
	public void stoppen(char taste) {
		// TODO Auto-generated method stub

	}

}
