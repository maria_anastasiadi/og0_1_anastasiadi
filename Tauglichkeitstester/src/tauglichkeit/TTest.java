package tauglichkeit;

public abstract class TTest {

	// Attribute
	protected float ergebnis;
	protected boolean aktiv;
	protected TCanvas canvas;
	protected TRect rechteck;
	protected int xMitte;
	protected int yMItte;

	// Methoden
	public TTest(TCanvas canvas, TRect rechteck) {
		this.canvas = canvas;
		this.rechteck = rechteck;
		//this.canvas
		//xMitte = (Rechteck.Left+Rechteck.Right) / 2;
		//yMitte = (Rechteck.Top+Rechteck.Bottom) / 2;
		aktiv = false;
		ergebnis = 0;
	}
	
	protected void warten() {
		
	}
	
	public abstract String zeigeHilfe();
	
	public abstract void starten();
	
	public abstract void stoppen(char taste);
	
	public float getErgebnis() {
		return ergebnis;
	}
	
	public boolean isAktiv() {
		return aktiv;
	}
	
	
}
