package suchalgorithmen;

import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.HashSet;

public class Interpolationssuche {
	static int[] array;
	static Scanner scanner = new Scanner(System.in);
	static Random rand = new Random();
	static Set<Integer> schonDa = new HashSet<Integer>();

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// System.out.println("Gib eine Zahl ein: ");
		// int fr = scanner.nextInt();

		array = new int[20000000];
		int c;

		for (int i = 0; i < array.length; i++) {
			array[i] = i * 2;
			/*
			 * c = rand.nextInt(30000000); if (!schonDa.contains(c)) { array[i] = c;
			 * schonDa.add(c); }else { i = i -1; }
			 */
		}

		long vorher;
		long nachher;
		int i = 0;

		while (i < 5) {
			int x = rand.nextInt(array.length * 2);
			System.out.println();
			System.out.println("gesuchte Zahl: " + x);
			System.out.println();
			System.out.println("Lineare Suche");
			vorher = System.currentTimeMillis();
			System.out.println(lineareSuche(0, array.length - 1, x));
			nachher = System.currentTimeMillis();
			System.out.println(nachher - vorher + " Millisekunden");

			System.out.println();
			System.out.println("Interpolationssuche");
			vorher = System.currentTimeMillis();
			System.out.println(interpolationssuche(0, array.length - 1, x));
			nachher = System.currentTimeMillis();
			System.out.println(nachher - vorher + " Millisekunden");
			i++;
		}
	}

	public static int lineareSuche(int anfang, int ende, int zahl) {
		for (int i = 0; i < ende; i++) {
			if (zahl == array[i]) {
				return i;
			}
		}
		return -1;
	}

	public static int interpolationssuche(int anfang, int ende, int zahl) {
		int m;
		int c = 0;

		while (anfang <= ende) {
			c++;
			m = anfang + ((ende - anfang) * ((zahl - array[anfang]) / (array[ende] - array[anfang])));
			int e = array[m];
			if (zahl == e) {
				return m;
			}
			if (zahl < e) {
				ende = m - 1;
			}
			if (zahl > e) {
				anfang = m + 1;
			}
		}
		return -1;

	}
}
