package suchalgorithmen;

public class Algorithmen {
	
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		long zahlen [];
		zahlen = new long [1000000];
		init (zahlen);
		int s;
		
		
	
		s = lineareSuche(zahlen,1999998);
		System.out.println(s);
		s = binaereSuche(zahlen,1999998);
		System.out.println(s);
		s = sssuche(zahlen,1999998);
		System.out.println(s);
		
	}


	public static void init (long [] z)
	{
		for (int i = 0; i<z.length;i++)
		{
			z[i] = i*2;
		}	
	}

public static int lineareSuche (long [] zahlen, long gesuchteZahl)
{
	int c = 0;
	
	for (int i = 0; i< zahlen.length;i++)
	{
		c++;
		if (zahlen [i] == gesuchteZahl)
		{
			System.out.println("Vergleiche:" + c);
			return i;		
		}
		
	}
	System.out.println("Vergleiche:" + c);
	return -1;
}


public static int binaereSuche (long [] zahlen, long gesuchteZahl)
{
	int l=0;
	int r = zahlen.length - 1;
	int m;
	int c = 0;
	
	while (l<=r)
	{
		m = l + ((r-l)/2);
		if (zahlen[m] == gesuchteZahl)
		{
			c++;
			System.out.println("Vergleiche:" + c);
			return m;
		}
		if (zahlen [m] > gesuchteZahl)
		{	
			c++;
			r = m-1;
		}
		
		else
		{
			c++;
			l = m+1;
		}
	}
	System.out.println("Vergleiche:" + c);
	return -1;
}

public static int sssuche (long [] zahlen, long gesuchteZahl)
{
	int c = 0;
	int l = zahlen.length / 4 -1;
	while (l<zahlen.length && l >=0)
	{
		if (gesuchteZahl == zahlen[l])
		{
			c++;
			System.out.println("Vergleiche:" + c);
			return l;
		}
		if (gesuchteZahl < zahlen[l])
		{
			c++;
			l--;
		}
		else
		{
			c++;
			l = l+ zahlen.length/4;
		}
		
	}
	System.out.println("Vergleiche:" + c);
	return -1;
}

}
