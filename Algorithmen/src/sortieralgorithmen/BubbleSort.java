package sortieralgorithmen;
import java.util.Arrays;

public class BubbleSort {
	private long vertauschungen = 0;

	public void sortiere(long[] zahlenliste) {
		int temp = 0;

		for (int n = zahlenliste.length - 1; n >= 1; n--) {
			for (int i = 0; i < n; i++){
				if (zahlenliste[i] > zahlenliste[i + 1]) {
					temp = (int) zahlenliste[i];
					zahlenliste[i] = zahlenliste[i + 1];
					zahlenliste[i + 1] = temp;
					inkrementVertauschungen();
				}

			}
			System.out.println(array2str(zahlenliste));
		}
	}

	private void inkrementVertauschungen() {
		vertauschungen++;
	}

	public long getVertauschungen() {
		return this.vertauschungen;
	}

	public static String array2str(long[] array) {
		String result = "";

		for (int i = 0; i < array.length; i++) {

			result = result + array[i] + "  ";

		}
		return result;
	}
	public static void main (String[]args){
		BubbleSort bs = new BubbleSort();
		
		//long[] zahlenliste = {5,4,8,3,9,7,6,2,1};
		//long[] zahlenliste = {1,2,3,4,5,6,7,8,9};
		//long[] zahlenliste = {9,8,7,6,5,4,3,2,1};
		long[] zahlenliste = {9,8,7,6,5,1,2,3,4};
		
		
		// Vor Sortierung
		System.out.println(Arrays.toString(zahlenliste));
		
		// Sortierung
		bs.sortiere(zahlenliste);
		
		// Nach Sortierung
		System.out.println(Arrays.toString(zahlenliste));
		System.out.println(bs.getVertauschungen());
	}
}
