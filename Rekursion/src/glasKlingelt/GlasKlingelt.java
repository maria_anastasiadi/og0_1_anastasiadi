package glasKlingelt;
import java.util.Scanner;

public class GlasKlingelt {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Wie viele Leute sollen anstoßen?: ");
		int eingabe = scanner.nextInt();
		System.out.println("Es klingelt: " + wieOftKlingelt(eingabe) + " mal");

	}
	
	
	
	
	public static int wieOftKlingelt(int n) {
		if (n == 1) {
			return 0;
		} else {
			return n -1 + wieOftKlingelt(n-1);
		}		
	}

}
