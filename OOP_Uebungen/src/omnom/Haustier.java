package omnom;

public class Haustier {
	private int hunger;
	private int muede;
	private int zufrieden;
	private int gesund;
	private String name;
	/*private int anzahl;
	private int dauerSchlafen;
	private int dauerSpielen;*/
	
public Haustier() {
	this.hunger = 100;
	this.muede = 100;
	this.zufrieden = 100;
	this.gesund = 100;
	this.name = "";
	
	
}

public Haustier(String name) {
	this.hunger = 100;
	this.muede = 100;
	this.zufrieden = 100;
	this.gesund = 100;
	this.name = name;
	
}

public int getHunger() {
	return hunger;
}

public void setHunger(int hunger) {
	this.hunger = hunger;
}

public int getMuede() {
	return muede;
}

public void setMuede(int muede) {
	this.muede = muede;
}

public int getZufrieden() {
	return zufrieden;
}

public void setZufrieden(int zufrieden) {
	this.zufrieden = zufrieden;
}

public int getGesund() {
	return gesund;
}

public void setGesund(int gesund) {
	this.gesund = gesund;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public void fuettern(int anzahl) {
	if (this.getHunger()<= 100) {
		this.setHunger (this.getHunger() + anzahl);		
	} 
}
public void schlafen(int dauerSchlafen) {
	if (this.getMuede()<= 100) {
		this.setMuede (this.getMuede() + dauerSchlafen);	
	}	
}
public void spielen(int dauerSpielen) {
	if (this.getZufrieden()<= 100) {
		this.setZufrieden (this.getZufrieden() + dauerSpielen);	
	}	
}
public void heilen() {
	if (this.getGesund()<100) {
		this.setGesund(100);
	} else {
		this.setGesund(0);
	}
			
}



}

