package arrayAufgabe;
import java.util.Scanner;

public class ArrayHelper {

public static String convertArrayToString (int [] zahlen) {
	String ausgabe = "";
	for (int i = 0; i < zahlen.length; i++) {
		if (i == 0) {
			ausgabe = ausgabe + zahlen[i];
		} else {
			ausgabe = ausgabe + " , " + zahlen[i];
		}
		
	}
	
	return ausgabe;
} 
public static String umwandeln (double [][] tab) {
	String ausgabe = "";
	for (int i = 0; i < tab[0].length; i++) {
		for (int j = 0; j < tab.length; j++) {
			ausgabe = ausgabe + tab[j][i] + "\t";
	
		}
		ausgabe = ausgabe + "\n";
	}
	
	return ausgabe;
}

public static void umdrehenArray(int[] zahlen) {
	
	int x;
	
	for (int i = 0; i < zahlen.length/2; i++) {
		x = zahlen[i];
		zahlen[i] = zahlen[zahlen.length - i - 1];
		zahlen[zahlen.length-i-1] = x;
		
	}
}

public static int[] neuesUmdrehenArray(int[] zahlen) {
	int[] neuesArray = new int[zahlen.length];
	for (int i = (zahlen.length - 1), k = 0; i >= 0; i--, k++) {
		neuesArray[k] = zahlen[i]; 
	}
	return neuesArray;
}

public static double[][] temperaturen(int laenge){
	double[][] tabelle = new double[2][laenge];
	
	tabelle[0][0] = 0.0;
	tabelle[0][1] = 10.0;
	tabelle[0][2] = 20.0;
	tabelle[0][3] = 30.0;
	tabelle[0][4] = 40.0;

	for (int i = 0; i < laenge; i++) {
		tabelle[1][i] = (tabelle[0][i]-32)*5/9;
		
	}
	return tabelle;
}

public static int[][] mxnMatrix (int m, int n){
	int[][] matrix = new int[m][n];
	Scanner myScanner = new Scanner(System.in);
	
	for (int i = 0; i < m; i++) { 
			for (int j = 0; j < n; j++) {
			System.out.print("Du befindest dich in: " + i + " : " + j);
			matrix[i][j] = myScanner.nextInt();  
			}
	}
	
	return matrix;
	
}

public static boolean checkMatrix (int[][] matrix) {
	boolean check = true;
	
	for (int i = 0; i < (matrix.length -1); i++) { 
		for (int j = 0; j < (matrix.length-1); j++) {
			if(matrix[i][j] != matrix[j][i]) {
				System.out.println("Fehler gefunden bei: " + i + " : " + j);
				check = false;
			}   
		}
}
	
	
	
return check;	
} 
}


